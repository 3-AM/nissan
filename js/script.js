 $(document).ready(function(){
    $(".category li").find('.popup_first').hide();
    $(".category li").find('.popup_second').hide();
    $(".category li").find('.popup_third').hide();
    $(".category li").find('.popup_fourth').hide();
    $(".category li").find('.popup_fifth').hide();
     $('ul.popup_first > li').find('.pop_div2').on('click', function(){
        $(this).next().slideToggle('fast');
        });
    $('ul.category > li').find('.pop_div3').on('click', function(){
        $(this).next().slideToggle('fast');
        });
    $('ul.category > li').find('.pop_div4').on('click', function(){
        $(this).next().slideToggle('fast');
        });
    $('ul.category > li').find('.pop_div5').on('click', function(){
        $(this).next().slideToggle('fast');
        });
    $('ul.category > li').find('.pop_div6').on('click', function(){
        $(this).next().slideToggle('fast');
        });

    $('ul.category > li').find('.pop_div1').on('click', function(){
        $(this).next().slideToggle('fast');
        $(this).find('img').toggleClass('sb_arrow');
        $(this).find('img').toggleClass('rotate');
        $(this).find('a').toggleClass('color');
        });
    $('ul.category a').click(function(e){
        e.preventDefault();
    });
    $('.category .pop_div2,.category .pop_div3,.category .pop_div4,.category .pop_div5,.category .pop_div6').click(function(){
        $(this).toggleClass('active')
    });

    $(".tabs_wrap .tabs_inset:first").addClass("tabs_active");
    $('.tabs_wrap').find('.tabs:not(:first)').hide();
    $('.tabs_wrap').find('.tabs_inset').click(function(){
    $(this).next('.tabs:visible').fadeIn('slow');
    $(this).next(".tabs").fadeIn('slow')
        .siblings(".tabs:visible").fadeOut();
    $(this).addClass("tabs_active");
    $(this).siblings(".tabs_inset").removeClass("tabs_active");
    });

    $('.catalog tr:odd').css('background', '#F2F2F2')
    $('.products_in').find('.issuance_list').addClass('issuance_active');
    $('.products').hide();
    $('.products_in').find('.issuance_table').on('click', function(){
        $(this).addClass('issuance_active');
        $('.products_in').find('.issuance_list').removeClass('issuance_active');
        $('.main_content').find('.catalog').hide();
        $('.products').fadeIn(700);
    });

    $('.products_in').find('.issuance_list').on('click', function(){
        $(this).addClass('issuance_active');
        $('.products_in').find('.issuance_table').removeClass('issuance_active');
        $('.main_content').find('.products').hide();
        $('.main_content').find('.catalog').fadeIn(700);
    });

    $("#example_2").ionRangeSlider({
        min: 1000,
        max: 50000,
        from: 5000,
        to: 30000,
        type: 'double',
        step: 1,
        postfix: " р",
        hasGrid: true
    });

});